<?php

use App\Http\Controllers\DeviceLeasingController;
use App\Http\Controllers\RegisterDeviceController;
use App\Http\Middleware\XApiKey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');


// /api/devices
Route::prefix('devices')->group(
    function () {
        Route::post('/register', [RegisterDeviceController::class, 'register']);
        Route::get('/info/{id}', [DeviceLeasingController::class, 'info'])->middleware(XApiKey::class);
    }
);

// /api/leasing
Route::prefix('leasing')->group(
    function () {
        Route::post('/update/{id}', [DeviceLeasingController::class, 'update'])->middleware(XApiKey::class);
    }
);
