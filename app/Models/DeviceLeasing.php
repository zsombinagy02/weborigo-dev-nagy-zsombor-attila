<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeviceLeasing extends Model
{
    use HasFactory;

    protected $table = 'device_leasings';
    protected $primaryKey = 'leasing_construction_id';
    protected $fillable = [
        'device_id',
        'device_owner',
        'leasing_construction_maximum_training',
        'leasing_construction_maximum_date',
        'leasing_construction_actual_period_start_date',
        'leasing_next_check',
        'device_trainings'
    ];
}
