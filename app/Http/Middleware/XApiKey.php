<?php

namespace App\Http\Middleware;

use App\Models\DeviceLeasing;
use App\Models\RegisterDevice;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class XApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        // Api key not given
        if (!$request->header('X-API-KEY')) {
            return response()->json([
                "title" => "missing parameter",
                "description" => "Authentication error."
            ], 400);
        }

        $api_key = $request->header('X-API-KEY');
        $device_id = $request->id;

        $register_device = RegisterDevice::where('device_id', $device_id)->first();

        if ($register_device) { // If the id in the request is for a device
            $device_api_key = $register_device->device_api_key;
        } else { // If the id in the request is for a leasing period
            $leasing_period = DeviceLeasing::where('leasing_construction_id', $request->id)->first();
            if (!$leasing_period) {
                return response()->json([
                    "title" => "not found.",
                    'description' => 'Period not found.'
                ], 400);
            }
            $register_device = RegisterDevice::where('device_id', $leasing_period->device_id)->first();
            $device_api_key = $register_device->device_api_key;
        }

        if ($api_key == $device_api_key) {
            return $next($request);
        } else {
            return response()->json([
                "title" => "unauthenticated.",
                'description' => 'Wrong api key provided.'
            ], 400);
        }
    }
}
