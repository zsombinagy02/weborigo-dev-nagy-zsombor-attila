<?php

namespace App\Http\Controllers;

use App\Models\DeviceLeasing;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\RegisterDevice;
use Illuminate\Http\JsonResponse;
use Carbon\Carbon;

class RegisterDeviceController extends Controller
{
    public function register(Request $request): JsonResponse
    {
        $device_id = $request->deviceId;
        $activation_code = $request->activationCode;

        $registration = RegisterDevice::where('device_id', $device_id)->first();

        if (!$registration) {
            return response()->json([
                "title" => "not found",
                "description" => "Device was not found in the database."
            ], 400);
        }

        /* 
        No activation code sent (could be assigned already), type is "unset"/"free" -> register to "free"
        API KEY LENGTH = 32
        Assuming registration without activation code results in the device_type becoming free
        */
        if (!$activation_code && ($registration->device_type == "unset" || $registration->device_type == "free")) {
            $registration->device_type = "free";
            $registration->device_api_key = Str::random(32);
            $registration->save();

            $leasing = DeviceLeasing::create([
                'device_id' => $registration->device_id,
                'device_owner' => "WebOrigo Magyarország Zrt.",
                'leasing_construction_maximum_training' => 50, // if type is free, training number is limited to 50
                'leasing_construction_actual_period_start_date',
                'leasing_construction_maximum_date' => now()->addMonth(), // if type is free, maximum date is limited to 1 month
            ]);
            $leasing->save();

            return response()->json([
                'deviceId' => $registration->device_id,
                'deviceAPIKey' => $registration->device_api_key,
                'deviceType' => $registration->device_type,
                'timestamp' => date('Y-m-d h:m:s')
            ], 201);
        }

        /*
        Valid activation code sent, 
        */
        if ($activation_code && !$registration->activation_code && ($registration->device_type === "unset" || $registration->device_type === "free")) {
            $registration->device_type = "leasing";
            $registration->device_api_key = Str::random(32);
            // Here we should check if activation code is valid
            $registration->activation_code = $activation_code;
            $registration->save();

            $leasing = DeviceLeasing::create([
                'device_id' => $registration->device_id,
                'device_owner' => "WebOrigo Magyarország Zrt.",
                'leasing_construction_maximum_training' => 1000, // if type is leasing, training number is limited to 1000
                'leasing_construction_actual_period_start_date',
                'leasing_construction_maximum_date' => now()->addYear(), // if type is free, maximum date is limited to 12 months
            ]);
            $leasing->save();

            return response()->json([
                'deviceId' => $registration->device_id,
                'deviceAPIKey' => $registration->device_api_key,
                'deviceType' => $registration->device_type,
                'timestamp' => date('Y-m-d h:m:s')
            ], 201);
        }

        return response()->json([
            "title" => "error",
            "description" => "Something went wrong, please try again."
        ], 400);
    }
}
