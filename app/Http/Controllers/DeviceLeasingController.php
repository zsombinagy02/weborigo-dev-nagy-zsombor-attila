<?php

namespace App\Http\Controllers;

use App\Models\DeviceLeasing;
use App\Models\RegisterDevice;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DeviceLeasingController extends Controller
{
    public function info(String $id, Request $request): JsonResponse
    {
        if (!$id) {
            return response()->json([
                "title" => "missing argument",
                "description" => "Device id was not given."
            ], 400);
        }

        $device = RegisterDevice::where('device_id', $id)->first();
        if (!$device) {
            return response()->json([
                "title" => "not found",
                "description" => "Device not found."
            ], 400);
        }

        if ($device->device_type != "leasing") {
            return response()->json([
                "deviceId" => $device->device_id,
                "deviceType" => "free",
                "leasingPeriods" => [],
                "timestamp" => date('Y-m-d h:m:s')
            ], 201);
        } else {
            $device_leasing = DeviceLeasing::where('device_id', $device->device_id)->orderBy('leasing_construction_actual_period_start_date', 'asc')->get();
            $actual_leasing = $device_leasing[0];
            $leasing_periods = [];

            foreach ($device_leasing as $dl) {
                array_push($leasing_periods, [
                    "leasingConstructionId" => $dl->leasing_construction_id,
                    "leasingConstructionMaximumTraining" => $dl->leasing_construction_maximum_training,
                    "leasingConstructionMaximumDate" => $dl->leasing_construction_maximum_date,
                ]);
            }

            return response()->json([
                "deviceId" => $device->device_id,
                "deviceType" => $device->device_type,
                "deviceOwner" => $device->device_owner,
                "dateOfRegistration" => $device->date_of_registration,
                "timestamp" => date('Y-m-d h:m:s'),
                "leasingPeriodsComputed" => [
                    "leasingConstructionId" => $actual_leasing->leasing_construction_id,
                    "leasingConstructionMaximumTraining" => $actual_leasing->leasing_construction_maximum_training,
                    "leasingConstructionMaximumDate" => $actual_leasing->leasing_construction_maximum_date,
                    "leasingActualPeriodStartDate" => $actual_leasing->leasing_construction_actual_period_start_date,
                    "leasingNextCheck" => $actual_leasing->leasing_next_check
                ],
                "leasingPeriods" => $leasing_periods
            ], 201);
        }

        return response()->json([
            "title" => "error",
            "description" => "Something went wrong, please try again."
        ], 400);
    }

    public function update(String $id, Request $request): JsonResponse
    {
        $current_leasing = DeviceLeasing::where('leasing_construction_id', $id)->first();

        if (!$current_leasing) {
            return response()->json([
                "title" => "not found",
                "description" => "Device not found."
            ], 400);
        }
        //Not sure how we handle this value or when it increments
        $current_leasing->device_trainings = $current_leasing->device_trainings + 1;
        $current_leasing->save();

        return response()->json([
            "deviceId" => $current_leasing->device_id,
            "deviceTrainings" => $current_leasing->device_trainings
        ], 201);
    }
}
