<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('register_devices', function (Blueprint $table) {
            $table->id();
            $table->string('device_id');
            $table->string('device_api_key');
            $table->string("device_type")->default("unset");
            $table->string("activation_code")->nullable();
            $table->timestamp('date_of_registration')->default(date('Y-m-d h:m:s'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('register_devices');
    }
};
