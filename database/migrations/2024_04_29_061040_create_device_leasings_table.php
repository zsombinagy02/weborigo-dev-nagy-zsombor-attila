<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('device_leasings', function (Blueprint $table) {
            $table->bigIncrements('leasing_construction_id');
            $table->string('device_id');
            $table->string('device_owner');
            $table->integer('leasing_construction_maximum_training')->nullable();
            $table->timestamp('leasing_construction_actual_period_start_date')->default(date('Y-m-d h:m:s'));
            $table->timestamp('leasing_construction_maximum_date')->nullable();
            $table->timestamp('leasing_next_check')->default(DB::raw('DATE_ADD(leasing_construction_actual_period_start_date, INTERVAL 1 DAY)'));
            $table->integer('device_trainings')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('device_leasings');
    }
};
