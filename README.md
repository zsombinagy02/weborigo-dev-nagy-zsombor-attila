<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

_All of my thoughts and questions are commented in the code_

`composer i`
--------
Linux/Git bash:
`cp .env.example .env`

Windows
`copy and paste the .env.example file and rename it to .env`
--------
Setup the database connection in .env

`php artisan key:generate`
`php artisan migrate (no factories were created so the data has to be filled by hand)` 
`php artisan serve`

After the setup you can try the api routes in postman.

You need to provide the X_API_KEY in authorization for the following routes
 - /api/devices/info/{id}
 - /api/leasing/update/{currentLeasingId}

For the following route you need to provide the body in raw JSON
 - /api/devices/register

Versions: 
- PHP – v 8.2.12
- Laravel – v 11.^